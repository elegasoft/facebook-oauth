<?php

namespace Elegasoft\FacebookOauth;

class FBOAuth
{

  public $graph;

  function __construct()
  {
    $fbSdk = app(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk::class);
    $config = ['persistent_data_handler' => 'session'];
    if(isset($_SESSION['fb_user_access_token'])){
      $config['default_access_token'] = (string) $_SESSION['fb_user_access_token'];
    }
    $this->graph = $fbSdk->newInstance($config);
  }

  public static function test()
  {
    echo "Test";
    return "Testing";
  }

}
