<?php

namespace Elegasoft\FacebookOauth;

class Facade extends \Illuminate\Support\Facades\Facade
{

  protected static function getFacadeAccessor()
  {
    return FBOAuth::class;
  }
}


 ?>
