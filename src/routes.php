<?php
session_start();

Route::group(['prefix'=>'fboauth'], function(){

  Route::get('login', function (\Elegasoft\FacebookOauth\FBOAuth $fb) {

    // Send an array of permissions to request
    $login_url = $fb->graph->getLoginUrl(['email']);
      return view('fboauth::login', compact('login_url'));
  })->name('fboauth.login');


});

Route::group(['prefix'=>'facebook'], function(){

  Route::get('callback', function(\Elegasoft\FacebookOauth\FBOAuth $oauth)
  {
    $fb = $oauth->graph;
    try {
        $token = $fb->getAccessTokenFromRedirect();
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
        dd(request(),$e->getMessage(),session());
    }

    // Access token will be null if the user denied the request
    // or if someone just hit this URL outside of the OAuth flow.
    if (! $token) {
        // Get the redirect helper
        $helper = $fb->getRedirectLoginHelper();

        if (! $helper->getError()) {
            abort(403, 'Unauthorized action.');
        }

        // User denied the request
        dd(
            $helper->getError(),
            $helper->getErrorCode(),
            $helper->getErrorReason(),
            $helper->getErrorDescription()
        );
    }

    if (! $token->isLongLived()) {
        // OAuth 2.0 client handler
        $oauth_client = $fb->getOAuth2Client();

        // Extend the access token.
        try {
            $token = $oauth_client->getLongLivedAccessToken($token);
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }
    }

    $fb->setDefaultAccessToken($token);

    // Save for later
    $_SESSION['fb_user_access_token'] = $token;
    session('fb_user_access_token',$token->getValue());

    // Get basic info on the user from F\acebook.
    try {
        $response = $fb->get('/me?fields=id,name,email');
    } catch (Facebook\Exceptions\FacebookSDKException $e) {
        dd($e->getMessage());
    }
    \Session::save();
    return redirect()->route('facebook.login.success', compact('token'))->with('message', 'Successfully logged in with Facebook');
});


});
